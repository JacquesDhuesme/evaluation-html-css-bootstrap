# Evaluation HTML CSS Bootstrap

## Description du projet

Ce projet est réalisé dans le cadre d'une évalution Studi. Pour rappel l'objectif est de réaliser un site vitrine pour 
une association. Le site doit comporter une page vitrine qui présente l'association et une page actualités sur laquel 
on retrouve les événement de l'association.

J'ai fait le choix de prendre l'exemple des ***Scouts et Guides de France*** car ils fournissent une charte graphique 
complète et répondent naturellement au sujet. Je précise que l'intégralité des images et des textes utilisé sont soit
libre de droit, soit utilisé en accord avec l'association.

## Sources

- [ ] [Charte graphique SGDF](https://chefscadres.sgdf.fr/ressources/#/explore/file/45/)